from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.

class Story8UnitTest(TestCase):

    def test_ada_url_ga(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_ada_tulisan_judulnya_ga(self):
        response = Client().get('')
        self.assertContains(response, 'Ingin mencari buku apa?')
        self.assertEqual(response.status_code, 200)

    def test_html_nya(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_tabel_nya_ada_ga(self):
        response = Client().get('')
        content = response.content.decode('utf8')
        self.assertIn ("<table", content)
